pico-8 cartridge // http://www.pico-8.com
version 32
__lua__
--outdrive
--james stocks
function _init()
  speed=0.0
  tick=0
  xpos=64
  steer=0.0
  
  track1={{0,50},{-1,50},{0,50},{1,50},{0,100}}
  section=1
  distance_tick=100--ticks to increment distance
  distance=0
  total_distance=0
  curve=0
  curve_render=0--lag on drawing the curve
end

function _update60()
  tick+=1
  if(tick==32767)tick=0
  if(btn(5))then
    speed=min(200,max(speed+0.3,speed*1.004))
  elseif(btn(4))then
    speed=speed*0.95
  else
    speed=speed*0.99
  end
  if(btn(1))then
    steer=min(3,steer+0.10)
  elseif(btn(0))then
    steer=max(-3,steer-0.10)
  else
    steer=steer*0.5
    if(steer<0.3)steer=0 
  end
  xpos=flr(xpos+steer)
  
  if(tick%distance_tick==(distance_tick-1))then
    distance+=flr(speed)
    total_distance+=flr(speed)
  end
  if(distance>track1[section][2])then
    if(section<#track1)then
      section+=1
      distance=0
      curve=track1[section][1]
    else
      --todo: you're winner!
    end
  end
  
  xpos=xpos+(1*curve)
end

function _draw()
  --todo support multiple curve degrees
  if(curve==0)then
    if(curve_render>0)then
      curve_render-=1
    elseif(curve_render<0)then
      curve_render+=1
    end
  elseif(abs(curve_render)<64)then
    curve_render+=2*curve
  end
  rectfill(0,0,128,128,12)
  print("mph "..flr(speed),2,2,1)
  print("distance "..total_distance,2,10,1)
  print("curve "..curve,2,18,1)
  print("curve render "..curve_render,2,26,1)
  for i=64,128do
    --grass
    rectfill(0,i,128,i,11)
    --ratio for squishing distant lines
    p_ratio=i
    --curb
    --alternate red & white according to speed
    c=7+(((i+2)+((tick/180)*speed))%2)
    curve_offset=0
    if(i>=(65-abs(curve_render)))then
      curve_offset=curve_render/(2*(i-63))
    end
    rectfill(120-p_ratio-curve_offset,i,p_ratio+8-curve_offset,i,c)
    --road
    rectfill(124-p_ratio-curve_offset,i,p_ratio+4-curve_offset,i,0)
  end
  
  spr(0,xpos-8,116,2,1)
end
__gfx__
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00008888888800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00888888888888000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
08888888888888800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
08888825528888800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
08222222222222800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
01100000000001100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
