Map layer 0 = solid or walkable
Map layer 1 = tile has key. Subtract 1 to get same tile without key
Map layer 2 = key can be used. Subtract 1 for same tile unlocked


Game states
2 = playing
3 = game over
4 = end sequence


Save data
0 bones
1 player max HP
2 player attack
3 player bone rate
4 player bone bank

Save data on itch is at <randomstring>.ssl.hwcdn.net

# Max cost for bone_rate
If cost is 2.1*bone_rate*bone_rate then the cost overflows wildly when rate > root(32,768)/2.1 = 86.2
Therefore when bone_rate > 80, cost is just 32000 instead of calculating.